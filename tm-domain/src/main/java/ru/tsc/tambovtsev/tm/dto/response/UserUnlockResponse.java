package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable UserDTO user) {
        super(user);
    }
}

package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<TaskDTO> {

    void create(@NotNull TaskDTO task);

    void updateById(@NotNull TaskDTO task);

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<TaskDTO> findAllTask();

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    void clearTask();

}

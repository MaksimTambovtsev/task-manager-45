package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public UserService(@NotNull IConnectionService connection,
                       @Nullable final IUserRepository userRepository,
                       @NotNull IPropertyService propertyService,
                       @Nullable IProjectRepository projectRepository,
                       @Nullable ITaskRepository taskRepository) {
        super(userRepository, connection);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) return null;
            return user;
        }
        finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final UserDTO user = repository.findByEmail(email);
            if (user == null) return null;
            return user;
        }
        finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){entityManager.getTransaction().rollback();}
        finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final UserDTO user = repository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            entityManager.getTransaction().begin();
            repository.updateUser(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final UserDTO user = findByLogin(login);
            entityManager.getTransaction().begin();
            repository.lockUserById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final UserDTO user = findByLogin(login);
            entityManager.getTransaction().begin();
            repository.unlockUserById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public IRepository<UserDTO> getRepository(@NotNull EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @Override
    public void addAll(@NotNull final Collection<UserDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final List<UserDTO> result = repository.findAllUser();
            return result;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearUser();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

}
